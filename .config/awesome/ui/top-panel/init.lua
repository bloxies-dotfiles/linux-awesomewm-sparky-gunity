local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')
local naughty = require('naughty')

local configuration = require('configuration.config')
require('widgets.top-panel')

local TopPanel = function(s)

  -- Wiboxes are much more flexible than wibars simply for the fact that there are no defaults, however if you'd rather have the ease of a wibar you can replace this with the original wibar code
  local panel =
    wibox(
      {
        ontop = true,
        screen = s,
        height = configuration.toppanel_height,
        width = s.geometry.width,
        x = s.geometry.x,
        y = s.geometry.y,
        stretch = false,
        bg = beautiful.ui_normal,
        fg = beautiful.fg_normal,
        struts = {
          top = configuration.toppanel_height
        }
      }
    )

  panel:struts(
    {
      top = configuration.toppanel_height
    }
  )
  --

  client.connect_signal("manage", function (c)
                          -- Delay the check to allow the window to properly load on startup
                          gears.timer {
                            timeout   = 0.5,
                            autostart = true,
                            single_shot = true,
                            callback  = function()
                              if c.maximized then
                                awful.titlebar.hide(c)
                                c.shape = function(cr,w,h)
                                  gears.shape.partially_rounded_rect(cr, w, h, true, true, false, false, configuration.rounded_corner_size)
                                end

                                -- Little hack that automatically refreshes the struts, meaning that the position of windows updates allowing the maximized window to properly sync up, otherwise there is empty space left for the now absent titlebar
                                panel:struts({top = configuration.toppanel_height + 1})
                                panel:struts({top = configuration.toppanel_height})

                                panel:get_children_by_id("clientbuttons")[1].visible = true
                              else

                                if c.floating or c.first_tag.layout.name == "floating" then
                                  awful.titlebar.show(c)
                                end

                                c.shape = function(cr,w,h)
                                  gears.shape.rounded_rect(cr,w,h,configuration.rounded_corner_size)
                                end

                                panel:get_children_by_id("clientbuttons")[1].visible = false
                              end
                            end
                          }
  end)

  client.connect_signal("property::maximized", function (c)
                          if c.maximized then
                            awful.titlebar.hide(c)
                            c.shape = function(cr,w,h)
                              gears.shape.partially_rounded_rect(cr, w, h, true, true, false, false, configuration.rounded_corner_size)
                            end


                            -- Little hack that automatically refreshes the struts, meaning that the position of windows updates allowing the maximized window to properly sync up, otherwise there is empty space left for the now absent titlebar
                            panel:struts({top = configuration.toppanel_height + 1})
                            panel:struts({top = configuration.toppanel_height})

                            panel:get_children_by_id("clientbuttons")[1].visible = true
                          else

                            if c.floating or c.first_tag.layout.name == "floating" then
                              awful.titlebar.show(c)
                            end

                            c.shape = function(cr,w,h)
                              gears.shape.rounded_rect(cr,w,h,configuration.rounded_corner_size)
                            end

                            panel:get_children_by_id("clientbuttons")[1].visible = false
                          end
  end)

  client.connect_signal("focus", function(c)
                          if c.maximized then
                            panel:get_children_by_id("clientbuttons")[1].visible = true
                          else
                            panel:get_children_by_id("clientbuttons")[1].visible = false
                          end
  end)

  -- This delay might not be needed, must check later
  client.connect_signal("property::minimized", function(c)
                          gears.timer {
                            timeout   = 0.5,
                            autostart = true,
                            single_shot = true,
                            callback  = function()
                              if client.focus then
                                if client.focus.maximized then
                                  panel:get_children_by_id("clientbuttons")[1].visible = true
                                end
                              else
                                panel:get_children_by_id("clientbuttons")[1].visible = false
                              end
                            end
                          }
  end)

  -- This delay might not be needed, must check later
  client.connect_signal("unmanage", function(c)
                          gears.timer {
                            timeout   = 0.5,
                            autostart = true,
                            single_shot = true,
                            callback  = function()
                              if client.focus then
                                if client.focus.maximized then
                                  panel:get_children_by_id("clientbuttons")[1].visible = true
                                end
                              else
                                panel:get_children_by_id("clientbuttons")[1].visible = false
                              end
                            end
                          }
  end)


  tag.connect_signal("property::selected", function(t)
                       if not client.focus then
                         panel:get_children_by_id("clientbuttons")[1].visible = false
                       end
  end)

  panel:setup {
    {
      layout = wibox.layout.align.horizontal,
      { -- Left widgets
        layout = wibox.layout.fixed.horizontal,
        spacing = 18,
        {
          layout = wibox.layout.fixed.horizontal,
          killbutton,
          minimizebutton,
          maximizebutton,
          ontopbutton,
          stickybutton,
          visible = false,
          id = "clientbuttons",
        },
        clientname,
        s.mypromptbox,
      },
      s.mytasklist, -- Middle widget
      { -- Right widgets
        layout = wibox.layout.fixed.horizontal,
        s.mytaglist,
        mykeyboardlayout,
        wibox.widget.systray(),
        s.mylayoutbox,
      },
    },
    {
      {
        layout = wibox.layout.fixed.horizontal,
        mytextclock,
      },
      valign = "center",
      halign = "center",
      layout = wibox.container.place,
    },
    layout = wibox.layout.stack,
  }


  return panel
end

return TopPanel

