local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')

configuration = require('configuration.config')
require('widgets.top-panel')

local TopPanel = function(s)

  function toppanel_shape
    (cr, width, height)

    cr:move_to(0 , 0)

    cr:line_to(width , 0)

    cr:line_to(width , height)

    cr:curve_to(width , height , width , 0 , 0 , 0)

  end

  -- Wiboxes are much more flexible than wibars simply for the fact that there are no defaults, however if you'd rather have the ease of a wibar you can replace this with the original wibar code
  local panel =
    wibox(
      {
        ontop = true,
        visible = true,
        shape = toppanel_shape,
        screen = s,
        height = configuration.rounded_corner_size,
        width = configuration.rounded_corner_size,
        x = s.geometry.width - configuration.rounded_corner_size,
        y = s.geometry.y +  configuration.toppanel_height,
        stretch = false,
        bg = beautiful.ui_normal,
        fg = beautiful.fg_normal,
        struts = {
          top = configuration.toppanel_height
        }
      }
    )

  -- panel:struts(
  --   {
  --     top = configuration.toppanel_height
  --   }
  -- )
  -- --

  -- panel:setup {
  --   {
  --     layout = wibox.layout.align.horizontal,
  --     { -- Left widgets
  --       layout = wibox.layout.fixed.horizontal,
  --       mylauncher,
  --       s.mytaglist,
  --       s.mypromptbox,
  --     },
  --     s.mytasklist, -- Middle widget
  --     { -- Right widgets
  --       layout = wibox.layout.fixed.horizontal,
  --       mykeyboardlayout,
  --       wibox.widget.systray(),
  --       mytextclock,
  --       s.mylayoutbox,
  --     },
  --   },
  --   margins = 6,
  --   widget = wibox.container.margin,
  -- }


  return panel
end

return TopPanel

