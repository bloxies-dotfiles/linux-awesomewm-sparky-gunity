local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')

configuration = require('configuration.config')

clientname = wibox.widget{
  text = 'AwesomeWM Desktop',
  align  = 'center',
  valign = 'center',
  font = 'roboto bold 9',
  widget = wibox.widget.textbox
}

-- TODO Implement pcalls for when the classname is nil
client.connect_signal("focus", function(c)

                        if c then
                          -- Perhaps it would be better to pcall this and combine the two conditionals
                          local strout = c.class:gsub("^%l", string.upper)

                          if clientname.text ~= strout then
                            clientname.text = strout
                          end

                        end

end)

tag.connect_signal("property::selected", function(t)
                     if not client.focus then
                       clientname.text = "AwesomeWM Desktop"
                     end
end)

client.connect_signal("property::minimized", function(t)
                     if not client.focus then
                       clientname.text = "AwesomeWM Desktop"
                     end
end)

client.connect_signal("unmanage", function(t)
                        if not client.focus then
                          clientname.text = "AwesomeWM Desktop"
                        end
end)

