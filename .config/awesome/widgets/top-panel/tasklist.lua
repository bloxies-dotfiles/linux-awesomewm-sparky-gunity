local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")

local tasklist_buttons = gears.table.join(
  awful.button({ }, 1, function (c)
      if c == client.focus then
        c.minimized = true
      else
        c:emit_signal(
          "request::activate",
          "tasklist",
          {raise = true}
        )
      end
  end),
  awful.button({ }, 3, function()
      awful.menu.client_list({ theme = { width = 250 } })
  end),
  awful.button({ }, 4, function ()
      awful.client.focus.byidx(1)
  end),
  awful.button({ }, 5, function ()
      awful.client.focus.byidx(-1)
end))

-- Create a tasklist widget
-- This works, however it may be better to inherit the s variable from the top panel itself
awful.screen.connect_for_each_screen(function(s)
    s.mytasklist = awful.widget.tasklist {
      screen   = s,
      filter = function(c, screen)
        return not awful.widget.tasklist.filter.focused(c, screen) and awful.widget.tasklist.filter.currenttags(c, screen)
      end,
      buttons  = tasklist_buttons,
      layout   = {
        spacing_widget = {
          {
            forced_width  = 5,
            forced_height = 24,
            thickness     = 1,
            color         = '#777777',
            widget        = wibox.widget.separator
          },
          valign = 'center',
          halign = 'center',
          widget = wibox.container.place,
        },
        spacing = 1,
        layout  = wibox.layout.fixed.horizontal
      },
      -- Notice that there is *NO* wibox.wibox prefix, it is a template,
      -- not a widget instance.
      widget_template = {
        {
          wibox.widget.base.make_widget(),
          forced_height = 5,
          id            = 'background_role',
          widget        = wibox.container.background,
        },
        {
          {
            id     = 'client_name',
            text   = 'client',
            widget = wibox.widget.textbox,
          },
          margins = 5,
          widget  = wibox.container.margin
        },
        nil,
        -- TODO Implement pcalls for when the classname is nil
        create_callback = function(self, c, index, objects) --luacheck: no unused args
          self:get_children_by_id('client_name')[1].text = c.class:gsub("^%l", string.upper)
        end,
        layout = wibox.layout.stack,
      },
    }
end)

