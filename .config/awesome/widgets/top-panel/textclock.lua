local wibox = require("wibox")

-- Create a textclock widget
mytextclock = wibox.widget{
  font = 'roboto bold 9',
  widget = wibox.widget.textclock
}
