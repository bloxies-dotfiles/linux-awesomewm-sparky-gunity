local gears = require("gears")
local awful = require("awful")
local beautiful = require("beautiful")
local naughty = require("naughty")

local function set_wallpaper(s)
  -- Wallpaper
  if beautiful.wallpaper then
    local wallpaper = beautiful.wallpaper
    -- If wallpaper is a function, call it with the screen
    if type(wallpaper) == "function" then
      wallpaper = wallpaper(s)
    end
    gears.wallpaper.maximized(wallpaper, s, true)
  end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

-- Set the same wallpaper for each screen, you could change this to have different wallpapers on each screen.
awful.screen.connect_for_each_screen(function(s)
    set_wallpaper(s)
end)

-- Asynchronously spawn colterm, once completed save state of wallpaper, we use this state so we can automatically restart awesome to apply the Xresources and make sure we do not keep restarting awesome in a loop, we also check if the wallpaper was changed while awesome was already running. In the xinitrc and xprofile we remove the state and set it back to 0 to allow the automatic Xresources refresh on system login.
-- awful.spawn.easy_async_with_shell("go run " .. gears.filesystem.get_configuration_dir() .. "configuration/util/colterm.go -bg 1 " .. beautiful.wallpaper, function()

awful.spawn.easy_async_with_shell("wpg -s " .. beautiful.wallpaper, function()

                                    if gears.filesystem.file_readable(gears.filesystem.get_configuration_dir() .. "modules/wallpaper-state.lua") then
                                      state = io.open(gears.filesystem.get_configuration_dir() .. "modules/wallpaper-state.lua", "r")
                                      if (state:read("*all")) == ('"' .. beautiful.wallpaper .. '"') then
                                        state:close()
                                      else
                                        state:close()
                                        state = io.open(gears.filesystem.get_configuration_dir() .. "modules/wallpaper-state.lua", "w+")
                                        state:write((string.format("%q", beautiful.wallpaper)))
                                        state:close()
                                        awesome.restart()
                                      end
                                    else
                                      state = io.open(gears.filesystem.get_configuration_dir() .. "modules/wallpaper-state.lua", "w+")
                                      state:write((string.format("%q", beautiful.wallpaper)))
                                      state:close()
                                      awesome.restart()
                                    end

end)
