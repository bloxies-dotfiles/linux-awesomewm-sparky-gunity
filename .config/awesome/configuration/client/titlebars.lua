local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local naughty = require("naughty")
local configuration = require('configuration.config')


-- Double click titlebar timer, how long it takes for a 2 clicks to be considered a double click
function double_click_event_handler(double_click_event)
  if double_click_timer then
    double_click_timer:stop()
    double_click_timer = nil
    return true
  end
  
  double_click_timer = gears.timer.start_new(0.20, function()
                                               double_click_timer = nil
                                               return false
  end)
end


-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- original buttons for the titlebar, if you'd rather not have to double click functionality
    -- local buttons = gears.table.join(
    --     awful.button({ }, 1, function()
    --         c:emit_signal("request::activate", "titlebar", {raise = true})
    --         awful.mouse.client.move(c)
    --     end),
    --     awful.button({ }, 3, function()
    --         c:emit_signal("request::activate", "titlebar", {raise = true})
    --         awful.mouse.client.resize(c)
    --     end)
    -- )

    -- new buttons for the titlebar, this allows you to double click and toggle maximization of client
    local buttons = awful.util.table.join(
      buttons,
      awful.button({ }, 1, function()
          c:emit_signal("request::activate", "titlebar", {raise = true})
          -- WILL EXECUTE THIS ON DOUBLE CLICK
          if double_click_event_handler() then
            c.maximized = not c.maximized
            c:raise()
          else
            awful.mouse.client.move(c)
          end
      end),
      awful.button({ }, 3, function()
          c:emit_signal("request::activate", "titlebar", {raise = true})
          awful.mouse.client.resize(c)
      end)
    )


    awful.titlebar(c) : setup {
      {
        { -- Right
          awful.titlebar.widget.closebutton    (c),
          awful.titlebar.widget.minimizebutton    (c),
          awful.titlebar.widget.maximizedbutton(c),
          -- awful.titlebar.widget.floatingbutton (c),
          awful.titlebar.widget.stickybutton   (c),
          awful.titlebar.widget.ontopbutton    (c),
          layout = wibox.layout.fixed.horizontal()
        },
        {
          widget = wibox.widget.separator,
          buttons = buttons,
          visible = false,
        },
        { -- Left
          awful.titlebar.widget.iconwidget(c),
          buttons = buttons,
          layout  = wibox.layout.fixed.horizontal
        },
        layout = wibox.layout.align.horizontal
      },
      { -- Middle
        {
          { -- Title
            align  = "center",
            widget = awful.titlebar.widget.titlewidget(c)
          },
          -- buttons = buttons,
          layout  = wibox.layout.flex.horizontal
        },
        valign = "center",
        halign = "center",
        layout = wibox.container.place
      },
      layout = wibox.layout.stack
                              }

    c.shape = function(cr,w,h)
      gears.shape.rounded_rect(cr,w,h,configuration.rounded_corner_size)
    end


end)

-- Titlebars only on floating windows
client.connect_signal("property::floating", function(c)
                        if c.floating then
                          if not c.maximized then
                            awful.titlebar.show(c)
                          end
                        else
                          awful.titlebar.hide(c)
                        end
end)

function dynamic_title(c)
  if c.floating or c.first_tag.layout.name == "floating" then
    if not c.maximized then
      awful.titlebar.show(c)
    end
  else
    awful.titlebar.hide(c)
  end
end

tag.connect_signal("property::layout", function(t)
                     local clients = t:clients()
                     for k,c in pairs(clients) do
                       if c.floating or c.first_tag.layout.name == "floating" then
                         if not c.maximized then
                           awful.titlebar.show(c)
                         end
                       else
                         awful.titlebar.hide(c)
                       end
                     end
end)

-- -- WAS PROVEN UNNECESSARY/DOES NOT BELONG IN THIS MODULE

-- client.connect_signal("property::minimized", function(c)
--                         currentscreen = awful.screen.focused()

--                         local previous_client = awful.client.focus.history.get(s, 1)
--                         for _, c in ipairs(mouse.screen.selected_tag:clients()) do
--                           if #currentscreen.clients > 2 then
--                             c.minimized = true
--                           else
--                             c.minimzed = false
--                           end
--                         end
-- end)

client.connect_signal("manage", dynamic_title)

client.connect_signal("tagged", dynamic_title)
